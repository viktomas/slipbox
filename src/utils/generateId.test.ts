import * as assert from 'assert';
import { generateId } from './generateId';

describe('generateId', () => {
  it('should generateId as a concatenation of padded datetime values', () => {
    assert.strictEqual(generateId(new Date('2021-01-02T13:01:09')), '20210102130109');
  });
});
