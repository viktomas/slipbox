import * as vscode from 'vscode';
import * as path from 'path';

export default class LinkCompletionProvider implements vscode.CompletionItemProvider {
  private isWikiLinkStart(line: string): boolean {
    const sinceLastClose = line.split(']').pop();
    return Boolean(sinceLastClose && sinceLastClose.indexOf('[') >= 0);
  }

  private getSearchExpression(line: string): string {
    const sinceLastClose = line.split(']').pop();
    const result = sinceLastClose?.split('[').pop();
    return result || '';
  }

  async provideCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    token: vscode.CancellationToken,
    context: vscode.CompletionContext,
  ): Promise<vscode.CompletionItem[] | vscode.CompletionList> {
    const linePrefix = document.lineAt(position).text.substr(0, position.character);
    const lineSuffix = document.lineAt(position).text.substr(position.character);

    if (!this.isWikiLinkStart(linePrefix)) {
      return [];
    }

    if (!vscode.window.activeTextEditor?.document.uri) {
      return [];
    }
    const currentDocumentUri: vscode.Uri = vscode.window.activeTextEditor?.document.uri;

    const expression = this.getSearchExpression(linePrefix);

    const symbols: vscode.SymbolInformation[] =
      (await vscode.commands.executeCommand(
        'vscode.executeWorkspaceSymbolProvider',
        `# ${expression}`,
      )) || [];

    return new vscode.CompletionList(
      symbols
        .filter((s) => s.name.startsWith('# ')) // remove symbols that are not H1 headings
        .map((symbol) => {
          const itemName = symbol.name.replace('# ', '');
          const item = new vscode.CompletionItem(itemName, vscode.CompletionItemKind.Text);
          const relativePath = path
            .relative(currentDocumentUri.toString(), symbol.location.uri.toString())
            .replace('../', '');
          item.insertText = `${itemName}](${relativePath})`;
          if (lineSuffix.startsWith(']')) {
            item.range = new vscode.Range(
              new vscode.Position(position.line, position.character - expression.length),
              new vscode.Position(position.line, position.character + 1),
            );
          }
          return item;
        }),
      false,
    );
  }
}
