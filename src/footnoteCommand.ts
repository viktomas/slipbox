import * as vscode from 'vscode';
import { getFootnoteMetadata } from './footnoteMetadata';
import { Footnotes } from './footnotes';

export default async function addFootnote() {
  const editor = vscode.window.activeTextEditor;
  if (!editor) {
    console.error('no open editor');
    return;
  }
  const footnoteUrl = await footnoteUrlDialogue();
  if (!footnoteUrl) {
    console.error('no URL entered');
    return;
  }
  const { title } = (await getFootnoteMetadata(footnoteUrl)) || {};
  const nextId = new Footnotes(editor.document.getText()).nextFootnoteIndex();
  await editor.edit((editBuilder) => {
    editBuilder.insert(editor.selection.start, `[^${nextId}]`);

    const lastLine = editor.document.lineAt(editor.document.lineCount - 1);
    const footnoteText = `[^${nextId}]: [${title}](${footnoteUrl})\n`;

    if (lastLine.isEmptyOrWhitespace) {
      editBuilder.insert(lastLine.range.start, footnoteText);
    } else {
      editBuilder.insert(lastLine.range.end, `\n${footnoteText}`);
    }
  });
}

const footnoteUrlDialogue = async (): Promise<string | undefined> => {
  let question = `What's the source URL?`;

  let footnoteUrl = await vscode.window.showInputBox({
    prompt: question,
  });

  return footnoteUrl;
};
