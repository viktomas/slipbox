import * as assert from 'assert';
import { getFormattedDate } from './getFormattedDate';

describe('getFormattedDate', () => {
  it('should format the date as YYYY-MM-DD', () => {
    assert.strictEqual(getFormattedDate(new Date('2021-06-01')), '2021-06-01');
  });
});
