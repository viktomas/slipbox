import * as vscode from 'vscode';
import WikiLinkProvider from './wikiLinkProvider';
import WikiUriHandler from './wikiUriHandler';
import LinkCompletionProvider from './linkAutoCompletion';
import { saveNewNote } from './newNote';
import addFootnote from './footnoteCommand';
import { INSERT_URL_FOOTNOTE, RENAME, SAVE_NEW_NOTE } from './commandNames';
import { rename } from './renameCommand';

export function activate(context: vscode.ExtensionContext) {
  context.subscriptions.push(vscode.commands.registerCommand(SAVE_NEW_NOTE, saveNewNote));

  context.subscriptions.push(vscode.commands.registerCommand(RENAME, rename));

  const selector: vscode.DocumentSelector = { language: 'markdown', scheme: '*' };

  let linkProviderDisposable = vscode.languages.registerDocumentLinkProvider(
    selector,
    new WikiLinkProvider(),
  );
  context.subscriptions.push(linkProviderDisposable);

  let handlerDisposable = vscode.window.registerUriHandler(new WikiUriHandler());
  context.subscriptions.push(handlerDisposable);

  let providerDisposable = vscode.languages.registerCompletionItemProvider(
    'markdown',
    new LinkCompletionProvider(),
    '[',
  );
  context.subscriptions.push(providerDisposable);
  context.subscriptions.push(vscode.commands.registerCommand(INSERT_URL_FOOTNOTE, addFootnote));
}

// this method is called when your extension is deactivated
export function deactivate() {}
