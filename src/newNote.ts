import * as vscode from 'vscode';
import * as path from 'path';
import { TextEncoder } from 'util';
import { assert } from 'console';
import { getCurrentFileTitle } from './utils/getCurrentFileTitle';
import { toLowerSnakeCase } from './utils/toLowerSnakeCase';
import { generateId } from './utils/generateId';
import { getFormattedDate } from './utils/getFormattedDate';

const createFile = async (
  folder: string,
  filename: string,
  content: string,
): Promise<vscode.Uri> => {
  assert(vscode.workspace.rootPath);
  const fullPath = vscode.Uri.parse(path.join(vscode.workspace.rootPath || '', folder, filename));
  await vscode.workspace.fs.writeFile(fullPath, new TextEncoder().encode(content));
  return fullPath;
};

export async function saveNewNote() {
  const folders = vscode.workspace.getConfiguration('slipbox').folders;
  const folder = await vscode.window.showQuickPick(folders);
  if (!folder) return;
  await saveNoteToFolder(folder);
}

export async function saveNoteToFolder(folder: string) {
  if (!vscode.workspace.workspaceFolders?.length) {
    return;
  }

  const { activeTextEditor } = vscode.window;

  if (!activeTextEditor || !activeTextEditor.document.isUntitled) {
    return;
  }
  const { document } = activeTextEditor;
  const title = getCurrentFileTitle();

  const filenameTitle = title ? `-${toLowerSnakeCase(title)}` : '';
  const filename = `${generateId()}${filenameTitle}.md`;
  const text = document.getText();
  const textOrDefault = text || `# ${getFormattedDate()}\n\n`;
  const fullPath = await createFile(folder, filename, textOrDefault);
  await activeTextEditor.edit((eb) =>
    eb.delete(
      new vscode.Range(new vscode.Position(0, 0), document.positionAt(document.getText().length)),
    ),
  );
  await vscode.commands.executeCommand('workbench.action.closeActiveEditor');
  await vscode.commands.executeCommand('vscode.open', fullPath);
  await vscode.commands.executeCommand('cursorBottom');
}
