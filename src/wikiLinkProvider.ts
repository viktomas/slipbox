import * as vscode from 'vscode';
import parseWikiLinks from './wikiLinkParser';

export default class WikiLinkProvider implements vscode.DocumentLinkProvider {
  provideDocumentLinks(
    document: vscode.TextDocument,
    token: vscode.CancellationToken,
  ): vscode.ProviderResult<vscode.DocumentLink[]> {
    const text = document.getText();
    return parseWikiLinks(text).map((link) => {
      const uri = `${vscode.env.uriScheme}://viktomas.slipbox/${link.content}`;
      const documentLink = new vscode.DocumentLink(
        new vscode.Range(document.positionAt(link.start), document.positionAt(link.end)),
        vscode.Uri.parse(uri),
      );
      documentLink.tooltip = 'It works!';
      return documentLink;
    });
  }
}
