# slipbox

Slipbox is a VS Code extension that gives you Zettelkasten support. It allows you to create, link and rename Markdown notes.

**Warning**: this extension is in early development. I don't see how it could do anything to your files, but please back up (version) your files. Also, I might remove features in future versions.

## Configuration

There is only one configuration property:

- `slipbox.folders` (Default value: `["permanent", "reference"]`) - it contains a list of all folders where you store you Zettelkasten notes. I also use `"journal"` folder where I've got more ephemeral notes.

## Assumptions about your notes directory and file format

- You've got subfolders for reference notes and permanent notes. However, the extension should work well with all notes in the workspace folder.
- Notes are in the Markdown format
- Each note starts with a `# Title` (H1 markdown title)
- Note files have timestamp as a prefix (`20210401145812-note-name.md`)
- You link to notes using [markdown relative links](https://daringfireball.net/projects/markdown/syntax#link)

### Example

```txt
.
├── permanent/
│   ├── 20200605145305-permanent-notes.md
│   ├── 20200605150704-connections-between-notes.md
│   └── 20200605151143-literary-notes.md
└── reference/
    ├── 20200605153129-sonke-ahrens--how-to-take-smart-notes.md
    └── 20210403091115-ian-leslie--how-to-have-better-arguments-online.md
```

## Features

### Save new notes

Create a new note easily, slipbox will generate unique filename and uses your markdown title.

![Save new note](https://gitlab.com/viktomas/slipbox/-/raw/images/img/save-new-note.gif)

1. Open new `Untitled` editor (CMD/CTRL+N)
1. Start your note with a `#` heading, e.g

    ```md
    # Permanent notes

    Permanent notes are the main unit of information within the Slip-box.
    ```

1. After you finished the `#` heading, you can run the command `Slipbox: Save New Note` at any time.
1. Select one of your configured folders (e.g. `permanent`)
1. The extension will deduce the file name from the title and will save the note to the selected folder.

### Autocomplete links

Slipbox uses native VS Code symbol parsing for Markdown headings (the same as "Go to Symbol in Workspace" command). That allows for fast autocomplete. Start typing the title of the note you have in mind, and the extension will complete the title and file path for you.

![autocompletion gif](https://gitlab.com/viktomas/slipbox/-/raw/087ea0329211da1a02f693de1caa67e0f15d27eb/img/auto-completion.gif)

### Rename notes

Seamless global note rename based on its new Markdown title.

![Rename note](https://gitlab.com/viktomas/slipbox/-/raw/images/img/rename-note.gif)

1. Change the note title
1. Run `Slipbox: Rename note to match title` command
1. Slipbox renames the file and opens the search and replace panel with old and new filename prefilled so you can update all the links.

### Automatic footnotes from URL

![Footnote from URL](https://gitlab.com/viktomas/slipbox/-/raw/images/img/footnote-from-url.gif)

Command: `Slipbox: Insert Footnote from URL`. You enter URL and this extension automatically generates `[^1]` style footnote and fetches title from the referenced site.

## Goes well with

- [Markdown links](https://marketplace.visualstudio.com/items?itemName=tchayen.markdown-links)
- [Markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
