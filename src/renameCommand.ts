import * as vscode from 'vscode';
import * as path from 'path';
import * as assert from 'assert';
import { getCurrentFileTitle } from './utils/getCurrentFileTitle';
import { toLowerSnakeCase } from './utils/toLowerSnakeCase';

interface NoteFilename {
  id: string;
  name: string;
}

const getCurrentFileUri = (): vscode.Uri | undefined => {
  if (!vscode.workspace.workspaceFolders?.length) {
    return;
  }

  const { activeTextEditor } = vscode.window;

  if (!activeTextEditor || activeTextEditor.document.isUntitled) {
    return;
  }
  const { document } = activeTextEditor;
  return document.uri;
};

const parseFilename = (fullFilename: string): NoteFilename | undefined => {
  const result = fullFilename.match(/^(\d{14})-(.+)\.md$/);
  if (!result) {
    return;
  }
  const [, id, name] = result;
  return { id, name };
};

const getCurrentFileName = (): string | undefined => {
  const uri = getCurrentFileUri();
  if (!uri) return;
  return path.basename(uri.fsPath);
};

export const rename = async (): Promise<void> => {
  const newTitle = getCurrentFileTitle();
  const filename = getCurrentFileName();
  assert(filename, 'No open file');
  assert(newTitle, 'Open file does not have valid H1 markdown title');
  const noteFilename = parseFilename(filename);
  assert(
    noteFilename,
    'This file does not have valid note file name. (yyyymmddHHMMSS-snake-case-name.md)',
  );
  const newNoteName = toLowerSnakeCase(newTitle);
  if (noteFilename.name === newNoteName) {
    vscode.window.showWarningMessage('The file name is up to date');
    return;
  }
  const newFileName = `${noteFilename.id}-${newNoteName}.md`;
  const oldUri = getCurrentFileUri();
  assert(oldUri);
  const newUri = oldUri.with({ path: path.resolve(path.dirname(oldUri.path), newFileName) });
  const edit = new vscode.WorkspaceEdit();
  edit.renameFile(oldUri, newUri);
  await vscode.workspace.applyEdit(edit);
  await vscode.commands.executeCommand('workbench.action.findInFiles', {
    query: filename,
    replace: newFileName,
  });
};
