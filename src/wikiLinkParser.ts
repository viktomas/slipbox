export interface ParsedLink {
  start: number;
  end: number;
  content: string;
}

// inspired by: https://github.com/microsoft/vscode/blob/master/extensions/markdown-language-features/src/features/documentLinkProvider.ts#L50-L61
function matchAll(pattern: RegExp, text: string): Array<RegExpMatchArray> {
  const out: RegExpMatchArray[] = [];
  pattern.lastIndex = 0;
  let match: RegExpMatchArray | null;
  while ((match = pattern.exec(text))) {
    out.push(match);
  }
  return out;
}

const wikiLinkPattern = /\[\[([^\]]+)\]\]/g;

const parseWikiLinks = (text: string): ParsedLink[] => {
  const matches = matchAll(wikiLinkPattern, text);
  return matches.map((match) => {
    const offset = (match.index || 0) + 2;
    const start = offset;
    const end = offset + match[1].length;
    const content = match[1];
    return { start, end, content };
  });
};

export default parseWikiLinks;
