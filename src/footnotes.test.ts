import { Footnotes } from './footnotes';
import * as assert from 'assert';

describe('Footnotes', () => {
  it('parses a footnote', () => {
    const document = '[^1]';
    const footnotes = new Footnotes(document);
    assert.deepEqual(footnotes.footnotes, [{ index: 0, name: '1' }]);
  });

  it('parses multiple footnotes', () => {
    const document = 'hello is hello [^1]. And then some.\n foo [^hey]';
    const footnotes = new Footnotes(document);
    assert.deepEqual(footnotes.footnotes, [
      { index: 15, name: '1' },
      { index: 41, name: 'hey' },
    ]);
  });

  describe('nextFootnoteIndex', () => {
    it('starts with 1', () => {
      const document = 'no existing footnote';
      const footnotes = new Footnotes(document);
      assert.strictEqual(footnotes.nextFootnoteIndex(), 1);
    });
    it('returns index for the next footnote', () => {
      const document = 'hello is hello [^1].';
      const footnotes = new Footnotes(document);
      assert.strictEqual(footnotes.nextFootnoteIndex(), 2);
    });

    it('skips over empty spots', () => {
      const document = 'hello is hello [^1]. and [^3]';
      const footnotes = new Footnotes(document);
      assert.strictEqual(footnotes.nextFootnoteIndex(), 4);
    });
  });
});
