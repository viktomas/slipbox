const padStringRight = (initial: string, desiredLength: number, padString: string): string => {
  if (initial.length >= desiredLength) return initial;
  return padStringRight(`${padString}${initial}`, desiredLength, padString);
};

export const pad = (num: number, length: number): string => {
  return padStringRight(`${num}`, length, '0');
};
