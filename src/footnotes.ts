import * as assert from 'assert';

class Footnote {
  readonly index: number;
  readonly name: string;
  constructor(match: RegExpMatchArray) {
    assert(match.index !== undefined && match.index !== null);
    this.name = match[1];
    this.index = match.index;
  }
}

export class Footnotes {
  public footnotes: Footnote[];

  constructor(document: string) {
    this.footnotes = this.parseFootnotes(document);
  }

  private parseFootnotes(document: string): Footnote[] {
    const matches: RegExpExecArray[] = [];
    const regexp = /\[\^([^\]]+)\]/g;
    let result;
    do {
      result = regexp.exec(document);
      if (result) {
        matches.push(result);
      }
    } while (result);

    return matches?.map((m) => new Footnote(m)) || [];
  }

  public nextFootnoteIndex(): number {
    return this.footnotes.reduce((largestIndex, footnote) => {
      const currentIndex = Number.parseInt(footnote.name);
      return currentIndex >= largestIndex ? currentIndex + 1 : largestIndex;
    }, 1);
  }
}
