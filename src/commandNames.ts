export const SAVE_NEW_NOTE = 'slipbox.saveNewNote';
export const INSERT_URL_FOOTNOTE = 'slipbox.insertUrlFootnote';
export const RENAME = 'slipbox.rename';
