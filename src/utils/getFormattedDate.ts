import { pad } from './pad';

export const getFormattedDate = (datetime: Date = new Date()) => {
  return `${datetime.getFullYear()}-${pad(datetime.getMonth() + 1, 2)}-${pad(
    datetime.getDate(),
    2,
  )}`;
};
