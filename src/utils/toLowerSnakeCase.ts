export const toLowerSnakeCase = (noteTitle: string): string => {
  return noteTitle
    .toLowerCase()
    .replace(/ /g, '-')
    .replace('--', '-')
    .replace(/[^\w-]/g, '');
};
