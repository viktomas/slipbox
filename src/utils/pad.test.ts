import * as assert from 'assert';
import { pad } from './pad';

describe('pad', () => {
  it('should pad number with zeroes', () => {
    assert.strictEqual(pad(4, 3), '004');
    assert.strictEqual(pad(123, 2), '123');
    assert.strictEqual(pad(123, 10), '0000000123');
  });
});
