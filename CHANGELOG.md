# Change Log

## 0.7.1 - 2021-06-13 (BREAKING CHANGE)

- Creating a generic way to save notes. The config now allows to set a list of folders where the notes are stored.
- There is a new command `Slipbox: Save New Note` that replaces the `Slipbox: Save as Permanent Note` and `Slipbox: Save as Reference Note`.
- Look for the `Slipbox: Save New Note` command in README

*(note: release 0.7.0 had initialization issue)*

## 0.6.2 - 2021-04-18

- The process fo creating a new note has changed (you create untitled note first and then save it)
- Renaming notes based on their markdown title

## 0.5.0 - 2021-02-06

- Dialogue for setting a new note name is persistent (it doesn't disappear when it looses focus)

## 0.4.0 - 2020-08-13

**Breaking change removing unused command, but still without 1.0.0 commitment**.

- New command to create footnote from URL (with automatic fetch of the page title)
- New command to create a reference note
- Removed insert ID command


## 0.3.0 - 2020-07-12

**Breaking change in autocompletion, but still without 1.0.0 commitment**.

- Autocompletion of markdown links

## 0.2.0 - 2020-06-18

- Command to create a new permanent note

## 0.1.1

- fix incorrect ID timestamp generating (was using day in the week)

## 0.1.0 - Initial release

- supports 3 basic operations
  - Generate timestamp ID
  - click through WikiLink
  - Autocomplete wikilinks
