import { pad } from './pad';

export const generateId = (datetime: Date = new Date()) => {
  return `${datetime.getFullYear()}${pad(datetime.getMonth() + 1, 2)}${pad(
    datetime.getDate(),
    2,
  )}${pad(datetime.getHours(), 2)}${pad(datetime.getMinutes(), 2)}${pad(datetime.getSeconds(), 2)}`;
};
