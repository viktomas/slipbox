import * as vscode from 'vscode';

const titleLine = /^# (.+)$/;

const getTitleFromDocument = (document: vscode.TextDocument): string | undefined => {
  const firstLine = document.lineAt(0).text;
  const match = firstLine.match(titleLine);
  if (!match) return undefined;
  return match[1];
};

export const getCurrentFileTitle = (): string | undefined => {
  if (!vscode.workspace.workspaceFolders?.length) {
    return;
  }

  const { activeTextEditor } = vscode.window;

  if (!activeTextEditor) {
    return;
  }
  const { document } = activeTextEditor;
  return getTitleFromDocument(document);
};
